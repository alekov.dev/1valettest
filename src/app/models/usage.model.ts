export default interface UsageItem {
  date: Date;
  value: number;
}

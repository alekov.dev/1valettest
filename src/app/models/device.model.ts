import UsageItem from "./usage.model";

export default interface Device {
  id: number;
  name: string;
  relatedDevices?: Device[];
  status: string;
  temperature?: number;
  temperatureUnit?: string;
  type: string;
  usage?: UsageItem[];
}

import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import Device from '../models/device.model';

const deviceList : Device[] = [
  {
    id:100,
    name:"Device 1",
    relatedDevices: [
      {
        id: 400,
        name:"Device 4",
        status: "Available",
        type: "phone",
      },
      {
        id: 200,
        name:"Device 2",
        status: "Offline",
        type: "tablet",
      },
      {
        id: 300,
        name:"Device 3",
        status: "Available",
        type: "desktop",
      }
    ],
    status: "Available",
    temperature: 10,
    temperatureUnit: "Celsius",
    type: "phone",
    usage: []
  },
  {
    id:200,
    name:"Device 2",
    relatedDevices: [
      {
        id: 100,
        name:"Device 1",
        status: "Available",
        type: "phone",
      },
      {
        id: 500,
        name:"Device 5",
        status: "Offline",
        type: "tablet",
      },
      {
        id: 300,
        name:"Device 3",
        status: "Available",
        type: "desktop",
      }
    ],
    status: "Offline",
    temperature: 20,
    temperatureUnit: "Celsius",
    type: "tablet",
    usage: []
  },
  {
    id:300,
    name:"Device 3",
    relatedDevices: [
      {
        id: 100,
        name:"Device 1",
        status: "Available",
        type: "phone",
      },
      {
        id: 200,
        name:"Device 2",
        status: "Offline",
        type: "tablet",
      },
      {
        id: 600,
        name:"Device 6",
        status: "Available",
        type: "desktop",
      }
    ],
    status: "Available",
    temperature: 30,
    temperatureUnit: "Celsius",
    type: "desktop",
    usage: []
  }
];

@Injectable({
  providedIn: 'root'
})

export class HttpConfigInterceptor implements HttpInterceptor {

  constructor() { }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (req.url.endsWith('api/devices') && (req.method === 'GET')) {
      return new Observable(observer => {
        observer.next(new HttpResponse<Array<Device>>({body: deviceList, status: 200}))
      });
    } else if (req.url.includes('api/device') && (req.method === 'GET')) {
      // Get id from url
      const indexOfLastSlash = req.url.lastIndexOf('/');
      const id = +req.url.substr(indexOfLastSlash +1);
      
      const deviceDetails = deviceList.find(d => d.id === id)
      return new Observable(observer => {
        observer.next(new HttpResponse<Device>({body: deviceDetails, status: 200}))
      });
    }
    return next.handle(req);
  }
}
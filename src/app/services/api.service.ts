import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import Device from '../models/device.model';

const url = "http://myserver/api/";
@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }
  
  getDevices(): Observable<Device[]> {
    // TODO: Error handler should be implemented.
    return this.http.get<Device[]>(`${url}devices`);
  }

  getDeviceDetails(id: number): Observable<Device | undefined> {
    // TODO: Error handler should be implemented.
    return this.http.get<Device>(`${url}device/${id}`);
  }
}

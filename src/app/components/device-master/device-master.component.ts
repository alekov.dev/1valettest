import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-device-master',
  templateUrl: './device-master.component.html',
  styleUrls: ['./device-master.component.scss']
})
export class DeviceMasterComponent implements OnInit {
  @Input() id!: number;
  @Input() status!: string;
  @Input() name!: string;
  @Input() type!: string;

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  showDetails(id: number): void {
    this.router.navigate(["/details",id]);
  }
}

import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent implements OnInit {
  @Output() searchEvent = new EventEmitter<string>();

  searchValue: string = '';
  constructor() { }

  ngOnInit(): void {
  }

  onSearchChange (event: Event) {
    this.searchEvent.emit(this.searchValue);
  }

  clearSearch() {
    this.searchValue = '';
    this.searchEvent.emit(this.searchValue);
  }
}

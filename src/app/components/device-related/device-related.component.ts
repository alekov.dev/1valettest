import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-device-related',
  templateUrl: './device-related.component.html',
  styleUrls: ['./device-related.component.scss']
})
export class DeviceRelatedComponent implements OnInit {
  @Input() status!: string;
  @Input() name!: string;
  @Input() type!: string;

  constructor() { }

  ngOnInit(): void {
  }
}

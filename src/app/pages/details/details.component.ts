import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import Device from 'src/app/models/device.model';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit, OnDestroy {
  private subscription: Subscription;
  deviceDetails?: Device;

  constructor(private route: ActivatedRoute, private apiService: ApiService) { 
    this.subscription = new Subscription();
  }

  ngOnInit(): void {
    this.subscription.add(this.route.params.subscribe(params => {
      const id = params['id'];
      this.getDeviceDetails(id);
    }))
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  getDeviceDetails(id: number) {
    this.subscription.add(this.apiService.getDeviceDetails(id).subscribe(res => {
      this.deviceDetails = res;
    }));
  }
}

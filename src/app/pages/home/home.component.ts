import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import Device from 'src/app/models/device.model';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {
  devices : Device[] = [];
  filteredDevices: Device[] = [];
  subscription: Subscription;

  constructor(private apiService: ApiService) { 
    this.subscription = new Subscription();
  }

  ngOnInit(): void {
    this.getDevices();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  getDevices() {
    this.subscription.add(this.apiService.getDevices().subscribe(res => {
      this.devices = res;
      this.filterDevice('');
    }));
  }

  filterDevice(searchValue: string) {
    if(!this.filterDevice) {
      this.filteredDevices = this.devices;
    } else {
      this.filteredDevices = this.devices.filter(d => d.name.toLowerCase().includes(searchValue.toLowerCase()))
    }
  }

  searchEventHandler(searchValue: string) {
    this.filterDevice(searchValue);
  }
}

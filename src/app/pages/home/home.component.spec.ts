import { TestBed, async, fakeAsync, tick, waitForAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from "@angular/common/http/testing";
import * as Rx from 'rxjs';
import { HomeComponent } from './home.component';
import { ApiService } from 'src/app/services/api.service';
import { MatIconModule } from '@angular/material/icon';
import { FormsModule } from '@angular/forms';
import { SearchBarComponent } from 'src/app/components/search-bar/search-bar.component';
import Device from 'src/app/models/device.model';
import { By } from '@angular/platform-browser';
import { DeviceMasterComponent } from 'src/app/components/device-master/device-master.component';
import { DeviceIconComponent } from 'src/app/components/device-icon/device-icon.component';

const deviceList : Device[] = [
  {
    id:100,
    name:"Device 1",
    relatedDevices: [
      {
        id: 400,
        name:"Device 4",
        status: "Available",
        type: "phone",
      },
      {
        id: 200,
        name:"Device 2",
        status: "Offline",
        type: "tablet",
      },
      {
        id: 300,
        name:"Device 3",
        status: "Available",
        type: "desktop",
      }
    ],
    status: "Available",
    temperature: 10,
    temperatureUnit: "Celsius",
    type: "phone",
    usage: []
  },
  {
    id:200,
    name:"Device 2",
    relatedDevices: [
      {
        id: 100,
        name:"Device 1",
        status: "Available",
        type: "phone",
      },
      {
        id: 500,
        name:"Device 5",
        status: "Offline",
        type: "tablet",
      },
      {
        id: 300,
        name:"Device 3",
        status: "Available",
        type: "desktop",
      }
    ],
    status: "Offline",
    temperature: 20,
    temperatureUnit: "Celsius",
    type: "tablet",
    usage: []
  },
  {
    id:300,
    name:"Device 3",
    relatedDevices: [
      {
        id: 100,
        name:"Device 1",
        status: "Available",
        type: "phone",
      },
      {
        id: 200,
        name:"Device 2",
        status: "Offline",
        type: "tablet",
      },
      {
        id: 600,
        name:"Device 6",
        status: "Available",
        type: "desktop",
      }
    ],
    status: "Available",
    temperature: 30,
    temperatureUnit: "Celsius",
    type: "desktop",
    usage: []
  }
];

const deviceList2: Device[] = [];

describe('HomeComponent', () => {
  beforeEach(waitForAsync(() => {

    



    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
        MatIconModule,
        FormsModule,
      ],
      declarations: [
        HomeComponent,
        SearchBarComponent,
        DeviceMasterComponent,
        DeviceIconComponent
      ],
      providers : [
        ApiService
      ]
    }).compileComponents();
  }));

  it('all of the expected devices render on the home page', fakeAsync(() => {
    const fixture = TestBed.createComponent(HomeComponent);
    const component = fixture.debugElement.componentInstance;
    const service = fixture.debugElement.injector.get(ApiService);

    spyOn(service,"getDevices").and.callFake(() => {
      return Rx.of(deviceList)
    });
    component.getDevices();
    fixture.detectChanges()
    const debugTest = fixture.debugElement.queryAll(By.css('.device-card'));
    expect(component.devices.length).toEqual(debugTest.length);
  })) 
});
